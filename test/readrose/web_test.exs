defmodule Readrose.WebTest do
  use Readrose.DataCase

  alias Readrose.Web

  describe "posts" do
    alias Readrose.Web.Post

    @valid_attrs %{body: "some body", title: "some title", url: "some url"}
    @update_attrs %{body: "some updated body", title: "some updated title", url: "some updated url"}
    @invalid_attrs %{body: nil, title: nil, url: nil}

    def post_fixture(attrs \\ %{}) do
      {:ok, post} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Web.create_post()

      post
    end

    test "list_posts/0 returns all posts" do
      post = post_fixture()
      assert Web.list_posts() == [post]
    end

    test "get_post!/1 returns the post with given id" do
      post = post_fixture()
      assert Web.get_post!(post.id) == post
    end

    test "create_post/1 with valid data creates a post" do
      assert {:ok, %Post{} = post} = Web.create_post(@valid_attrs)
      assert post.body == "some body"
      assert post.title == "some title"
      assert post.url == "some url"
    end

    test "create_post/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Web.create_post(@invalid_attrs)
    end

    test "update_post/2 with valid data updates the post" do
      post = post_fixture()
      assert {:ok, %Post{} = post} = Web.update_post(post, @update_attrs)
      assert post.body == "some updated body"
      assert post.title == "some updated title"
      assert post.url == "some updated url"
    end

    test "update_post/2 with invalid data returns error changeset" do
      post = post_fixture()
      assert {:error, %Ecto.Changeset{}} = Web.update_post(post, @invalid_attrs)
      assert post == Web.get_post!(post.id)
    end

    test "delete_post/1 deletes the post" do
      post = post_fixture()
      assert {:ok, %Post{}} = Web.delete_post(post)
      assert_raise Ecto.NoResultsError, fn -> Web.get_post!(post.id) end
    end

    test "change_post/1 returns a post changeset" do
      post = post_fixture()
      assert %Ecto.Changeset{} = Web.change_post(post)
    end
  end
end
