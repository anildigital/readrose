defmodule ReadroseWeb.PageController do
  use ReadroseWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
