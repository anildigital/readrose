defmodule Readrose.Repo do
  use Ecto.Repo,
    otp_app: :readrose,
    adapter: Ecto.Adapters.Postgres
end
