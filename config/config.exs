# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :readrose,
  ecto_repos: [Readrose.Repo]

# Configures the endpoint
config :readrose, ReadroseWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "vqsBN7jikF4S9Pi8w5nfjP+utmUU47OolbvBZFcJe7OKjjcrJHxcFT+xUBoTgo+l",
  render_errors: [view: ReadroseWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Readrose.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
